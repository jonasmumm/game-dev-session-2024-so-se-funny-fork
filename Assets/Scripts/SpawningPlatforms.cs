using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawningPlatforms : MonoBehaviour
{
    public GameObject platformToSpawn;
    public float timer = 2f;

    float timeUntilSpawn;

    // Start is called before the first frame update
    void Start()
    {
        timeUntilSpawn = timer;
    }

    // Update is called once per frame
    void Update()
    {
        timeUntilSpawn = timeUntilSpawn - Time.deltaTime;
        if (timeUntilSpawn <= 0)
        {
            Instantiate(platformToSpawn);
            timeUntilSpawn = timer;
        }
    }
}
